window.__RUNTIME_CONFIG__ = {
  INDEXER__URL: "http://localhost:8000",
  CHAINBRIDGE: {
    chains: [
      {
        chainId: 195,
        networkId: 1987,
        name: "EtherGem",
        decimals: 18,
        bridgeAddress: "0xf2Db01B927C712baE5829BFD1B7E694b16F91fe1",
        erc20HandlerAddress: "0x4d6eEaC6912608529114Cc72b3fB6C27Fd812942",
        rpcUrl: "https://lb.rpc.egem.io",
        type: "Ethereum",
        nativeTokenSymbol: "EGEM",
        tokens: [
          {
            address: "0xE5fca20e55811D461800A853f444FBC6f5B72BEa",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
	  {
            address: "0xB6094af67bf43779ab704455c5DF02AD9141871B",
            name: "RUBY",
            symbol: "RUBY",
            imageUri: "WRUBYIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
	  {
            address: "0x33F4999ee298CAa16265E87f00e7A8671c01D870",
            name: "TrueUSD",
            symbol: "TUSD",
            imageUri: "TUSDIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0x97271a78C9778F226C1c4eFe63876c04f8cD8F40",
            name: "WrappedBNB",
            symbol: "WBNB",
            imageUri: "WBNBIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
        ],
      },
      {
        chainId: 56,
        networkId: 56,
        name: "Binance",
        decimals: 18,
        bridgeAddress: "0x5a4A110cF45D100FE0eA13b4aec043bBf4FFD0ff",
        erc20HandlerAddress: "0x531f6097E247FF6Ea92635fdB0Db8981aCD2f8D9",
        rpcUrl: "https://bsc-dataseed.binance.org/",
        type: "Ethereum",
        nativeTokenSymbol: "BNB",
        tokens: [
          {
            address: "0xF6AD9a164a0f1CfE5C3e7bc5c2B9E684de4aC9c5",
            name: "WrappedEGEM",
            symbol: "WEGEM",
            imageUri: "WEGEMIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce00",
          },
	  {
            address: "0x4d6eEaC6912608529114Cc72b3fB6C27Fd812942",
            name: "WrappedRUBY",
            symbol: "WRUBY",
            imageUri: "WRUBYIcon",
            resourceId:
              "0x195000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
	  {
            address: "0x14016e85a25aeb13065688cafb43044c2ef86784",
            name: "TrueUSD",
            symbol: "TUSD",
            imageUri: "TUSDIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce01",
          },
          {
            address: "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c",
            name: "WrappedBNB",
            symbol: "WBNB",
            imageUri: "WBNBIcon",
            resourceId:
              "0x560000000000000000000000000000c76ebe4a02bbc34786d860b355f5a5ce02",
          },
        ],
      },
    ],
  },
};
